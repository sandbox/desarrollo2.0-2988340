<?php

namespace Drupal\migrate_paragraphs\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Imports data into a paragraph item attached to a field.
 *
 * @code
 * Item example:
 *
 * process:
 *   new_text_field:
 *     plugin: paragraph_item
 *     source: paragraph
 *     paragraph_bundle: location_information
 *     paragraph_field: field_location_type
 *     paragraph_field_key: value
 *
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "paragraph_item",
 *   handle_multiples = FALSE
 * )
 */
class ParagraphItem extends ProcessPluginBase {

  const PARAGRAPH_ITEMS_LIST = self::class . '::paragraph_items';

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    $source_id = $this->getSourceIdUniqueValue($row);

    $paragraph_field = $this->configuration['paragraph_field'];
    $paragraph_field_key = !empty($this->configuration['paragraph_field_key']) ? $this->configuration['paragraph_field_key'] : NULL;

    // @TODO: handle delta control in separate method!
    $delta_paragraph = $this->getParagraphDelta($source_id, $paragraph_field);

    if (!is_array($value)) {
      $value = [$value];
    }

    foreach ($value as $delta => $item) {
      $paragraph = NULL;
      if (!is_null($item)) {
        if ($this->multiple() && !$this->splitOnMultiple()) {
          $paragraph = $this->getParagraph($source_id, $destination_property, $delta_paragraph);
          $value = !empty($paragraph_field_key) ? [$paragraph_field_key => $item] : $item;
          $paragraph->{$paragraph_field}->set($delta, $value);
          $paragraph->save();
        }
        else {
          $paragraph = $this->getParagraph($source_id, $destination_property, $delta);
          if (!empty($paragraph_field_key)) {
            $paragraph->{$paragraph_field}->{$paragraph_field_key} = $item;
          }
          else {
            if (is_array($item)) {
              foreach ($item as $property => $property_value) {
                $paragraph->{$paragraph_field}->{$property} = $property_value;
              }
            }
            else {
              $paragraph->{$paragraph_field} = $item;
            }
          }
          $paragraph->save();
        }
      }
    }

    if ($this->splitOnMultiple()) {
      $return = $this->getAllSourceParagraphs($source_id, $destination_property);
    }
    else {
      $paragraph = $this->getParagraph($source_id, $destination_property, $delta_paragraph);
      $return = $this->getParagraphIds($paragraph);
    }

    return $return;
  }

  /**
   * Get current delta of paragraph for the field.
   *
   * This is need to allow map multiple paragraph fields to the same paragraph.
   * It's being incremented for each migrate process action.
   *
   * @return int
   *   Current delta of paragraph.
   */
  public function getParagraphDelta($source_id, $paragraph_field) {
    static $delta_control = [];

    if (isset($delta_control[$source_id][$paragraph_field])) {
      $delta_control[$source_id][$paragraph_field]++;
    }
    else {
      $delta_control[$source_id][$paragraph_field] = 0;
    }
    return $delta_control[$source_id][$paragraph_field];
  }

  /**
   * Check if we should split each value provided in the plugin in paragraphs.
   */
  public function splitOnMultiple() {
    return isset($this->configuration['split_on_multiple']) ? $this->configuration['split_on_multiple'] : FALSE;
  }

  /**
   * Get source unique id value for the current row.
   *
   * Join all source ids available.
   *
   * @param \Drupal\migrate\Row $row
   *   Current row.
   *
   * @return string
   *   Source id unique value.
   */
  public function getSourceIdUniqueValue(Row $row) {
    $source_ids = $row->getSourceIdValues();
    return implode('-', $source_ids);
  }

  /**
   * Get paragraph ids for a specific paragraph.
   *
   * Used to return paragraph item values in migration.
   *
   * @return array
   *   Array key-value with paragraph id and revision id.
   */
  public function getParagraphIds(Paragraph $paragraph) {
    return [
      'target_id' => $paragraph->id(),
      'target_revision_id' => $paragraph->getRevisionId(),
    ];
  }

  /**
   * Get all paragraphs created in source.
   *
   * This is a workaround, needs a refactor!
   *
   * @param int $source_id
   *   The migration source id.
   * @param string $destination_property
   *   Paragraph field name where paragraphs will be created.
   *
   * @TODO: method to get all paragraphs instead of use count of values!
   */
  public function getAllSourceParagraphs($source_id, $destination_property) {
    $paragraphs = [];
    $paragraph_items = &drupal_static(self::PARAGRAPH_ITEMS_LIST, []);
    if (!empty($paragraph_items[$source_id][$destination_property])) {
      foreach ($paragraph_items[$source_id][$destination_property] as $paragraph) {
        $paragraphs[] = $this->getParagraphIds($paragraph);
      }
    }
    return $paragraphs;
  }

  /**
   * Gets the existing tab for a entity_id or create a new one.
   *
   * @todo support $delta.
   *
   * @param int $source_id
   *   The migration source id.
   * @param string $destination_property
   *   Paragraph field name where paragraphs will be created.
   * @param int $delta
   *   The delta for the paragraph.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null|static
   *   The paragraph tab.
   */
  protected function getParagraph($source_id, $destination_property, $delta = 0) {
    $paragraph_items = &drupal_static(self::PARAGRAPH_ITEMS_LIST, []);

    if (isset($paragraph_items[$source_id][$destination_property][$delta])) {
      return $paragraph_items[$source_id][$destination_property][$delta];
    }

    $paragraph_bundle = $this->configuration['paragraph_bundle'];

    $paragraph = Paragraph::create([
      'type' => $paragraph_bundle,
    ]);

    $paragraph_items[$source_id][$destination_property][$delta] = $paragraph;

    return $paragraph_items[$source_id][$destination_property][$delta];
  }

  /**
   * {@inheritdoc}
   */
  public function multiple() {
    $multiple = isset($this->configuration['is_multiple']) ? $this->configuration['is_multiple'] : FALSE;
    return $multiple;
  }

}
